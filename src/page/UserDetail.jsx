import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Card from "../component/Card";
import config from "../helper/config";
import DataUser from "../service/DataUser";

export default function UserDetail() {

    const [user, setUser] = useState({})
    const { userid } = useParams()

    useEffect(() => {

        const data = new DataUser(config.baseURL)
        data.getUser(userid)
            .then((res) => setUser(res.data))
            .catch((e) => console.log(e))

    }, [])



    return (
        <>
            <Card
                key={user.id}
                avatar={user.avatar}
                first_name={user.first_name}
                last_name={user.last_name}
                email={user.email} />
        </>
    )
}