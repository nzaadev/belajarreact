import { useEffect, useState } from 'react';
import DataUser from '../service/DataUser';
import Card from '../component/Card';
import Paging from '../component/Paging';
import config from '../helper/config';
import { Link } from 'react-router-dom';

function Home() {


    const [data, setData] = useState({})
    const [page, setPage] = useState(1)
    const [btnPaging, setBtnPaging] = useState({next: false, prev: true})

    useEffect(() => {

        const gempa = new DataUser(config.baseURL)
        gempa.getUsers(page).then((res) => setData(res))

        console.log("useeffct")


    }, [page])


    function nextPage() {
        if (page < data.total_pages) {
            setPage(page + 1)
            setBtnPaging({prev: false, next: true})
        } 



        
    }

    function prevPage() {
        if (page > 1) {
            setPage(page - 1)
            setBtnPaging({prev: true, next: false})
        } 

        
    }

    function info() {
        return `Current page: ${page} of ${data.total_pages}`
    }

    return (
        <>
            <div className='container'>
                {data.data !== undefined ? data.data.map((e) => {
                    return <Link to={`/users/${e.id}`} >
                        <Card
                            key={e.id}
                            avatar={e.avatar}
                            first_name={e.first_name}
                            last_name={e.last_name}
                            email={e.email} />
                    </Link>
                }) : "No data"}
            </div>
            <Paging isDisable={btnPaging} info={info} prev={prevPage} next={nextPage} />
        </>

    );
}

export default Home;
