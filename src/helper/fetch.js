async function provideRequestApi(url) {

    try {
        console.log("Fetch " + url)
        const f = await fetch(url)
        return await f.json()
    } catch (e) {
        console.log(e)
    }
}



export { provideRequestApi };