import './App.css';
import { Route, Routes } from 'react-router-dom';
import Home from './page/Home';
import Navbar from './component/Navbar';
import UserDetail from './page/UserDetail';

function App() {


  return (
    <>
      <Navbar/>
      <Routes>
        <Route path='/' element={<Home/>}/>
        <Route path='/users/:userid' element={<UserDetail/>}/>
      </Routes>
    </>
    
  );
}

export default App;
