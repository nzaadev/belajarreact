
import { provideRequestApi } from "../helper/fetch";

class DataUser {


    constructor(url) {
        this.url = url
    }


    async getUsers(page) {
        return await provideRequestApi(this.url + "users/?page=" + page)
    }

    async getUser(id) {
        return await provideRequestApi(this.url + "users/" + id)
    }
    
}

export default DataUser;