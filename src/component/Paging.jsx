export default function Paging({prev, next, info, isDisable}) {
    return (
        <div className="paging">
            <button disabled={isDisable.prev} onClick={prev} className="prev">&lt; Prev</button>
            <span>{info()}</span>
            <button disabled={isDisable.next} onClick={next} className="next">Next &gt;</button>
        </div>
    )
}