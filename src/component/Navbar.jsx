import { Link } from "react-router-dom";

export default function Navbar() {



    return (
        <nav>
            <b>User profile</b>
            <Link to='/'>Home</Link>
            <Link to='/profile'>Profile</Link>
            <Link to='/about'>About</Link>
        </nav>
    )
}