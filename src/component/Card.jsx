function Card(props) {
    return (
        <div className="box">
            <img className="img" style={{width: 100, height: 100}} src={props.avatar} alt=""/> <br />
            Name: {props.first_name} {props.last_name} <br />
            Email: {props.email}
        </div>
    )
}

export default Card;